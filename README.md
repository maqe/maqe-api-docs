**Table of contents**

- [MAQE API DOCS 📄📄](#maqe-api-docs-)
  - [ABOUT API DOCS 🤷](#about-api-docs-)
  - [STEP BY STEP [INSTALLATION] 👩‍🏫](#step-by-step-installation-)
  - [STEP BY STEP [DEVELOPMENT] 👩‍🏫](#step-by-step-development-)

# MAQE API DOCS 📄📄

**MAQE API DOCS** คือ package ที่จะทำให้ชีวิตการทำงานกับ Api docs สำหรับ Application ที่สร้างจาก laravel project ของคุณเป็นเรื่องที่ง่ายยิ่งขึ้น


## ABOUT API DOCS 🤷

มาทำความเข้าใจก่อนการใช้งาน MAQE API DOCS เริ่มกันเล้ย~

Maqe Api Docs จะเป็นการสร้าง docs แบบ specific หน่อย นั่นคือต้อง follow ตาม pattern ที่วางเอาไว้ซึ่งจะแบ่งออกเป็น 5 ส่วนหลักๆ

1. เราต้องกำหนดเมนูเอง ซึ่งเราสามารถไปกำหนดได้ที่ config file ที่ชื่อว่า `config/maqe-api-docs.php`
2. เราต้องสร้างไฟล์ blade เองโดยที่จะต้องสัมพันธ์กัน กับ config file (เดี๋ยวค่อยไปดูต่อที่หัวข้อ [Step By Step](#step_by_step))
3. เราต้องกำหนด Request parameter ในไฟล์ blade เอง โดยที่เราสามารถใช้ Request class เพื่อระบุ `Attributes` ได้เลยและสามารถเพิ่มเติม `queries` `Attributes` ได้ด้วย (เดี๋ยวค่อยไปดูต่อที่หัวข้อ [Step By Step](#step_by_step))
4. ในส่วนของ response สำหรับแต่ละ routes เราต้องทำการเขียน test เพื่อ generate response ข้อดีคือเป็นการ make sure ให้เราต้องเขียนเทสสำหรับ return ค่าด้วย สามารถดูตัวอย่างได้จากไฟล์ `tests/Feature/ExampleTest.php` หรือ เดี๋ยวค่อยไปดูต่อที่หัวข้อ [Step By Step](#step_by_step) ก็ได้
5. การอัพเดต version และวันที่ที่เราทำการอัพเดต ในส่วนนี้ เราต้อง manual พิมพ์คำสั่งเพื่อรัน หรือเราสามารถเรียก script ให้มันรันตอน deploy ก็ได้ 

## STEP BY STEP [INSTALLATION] 👩‍🏫

การใช้งานนะครับ

1. Install package มาใช้ใน project ที่เราทำงานอยู่ ดูวิธีได้จาก [ที่นี่](INSTALLATION.md)
2. run install command เพื่อ publish และ ติดตั้ง config ต่างๆที่จะเป็นโดยอัตโนมัติ

```sh
php artisan maqe-api-docs:install

# หลังจากที่เรารันคำสั่ง ระบบจะทำการสร้างไฟล์ต่าง และอัพเดตไฟล์ที่จำเป็นให้เราตาม path ด้านล่างนี้

# Script file...
./.scripts/gitlog.sh

# config file...
./config/maqe-api-docs.php

# view files...
./resources/views/maqe-api-docs/components/access_token.blade.php
./resources/views/maqe-api-docs/components/attribute.blade.php
./resources/views/maqe-api-docs/components/method.blade.php
./resources/views/maqe-api-docs/components/role.blade.php
./resources/views/maqe-api-docs/pages/exampleFolder
./resources/views/maqe-api-docs/pages/exampleFolder/examples.blade.php
./resources/views/maqe-api-docs/pages/examples.blade.php
./resources/views/maqe-api-docs/endpoint.blade.php
./resources/views/maqe-api-docs/index.blade.php
./resources/views/maqe-api-docs/layout.blade.php
./resources/views/maqe-api-docs/nav.blade.php

# tests file...
./tests/Feature/MaqeApiDocsExampleTest.php

# update route file...
./routes/web.php

```

3. จากนั้นให้รันคำสั่งเพื่อสร้าง `.gitlog` เพื่อนำไปใช้ระบุการอัพเดต api version ของเรา

```sh
sh ./.scripts/gitlog.sh
```

4. รันคำสั่ง test ไฟล์ `MaqeApiDocsExampleTest.php` ใน Test file ตัวนี้เราสามารถใช้ดูเป็นตัวอย่างสำหรับการสร้าง response json file ได้

```sh
php artisan test --filter MaqeApiDocsExampleTest
```

5. ทดสอบโดยการเข้าไปที่ route `docs` เช่น ถ้าหากคุณใช้ `php artisan serve` ในการ run project ก็ให้เข้าไปที่ [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)

## STEP BY STEP [DEVELOPMENT] 👩‍🏫

การสร้าง API ใหม่และการสร้าง Docs ของ api ที่เราสร้าง

- สร้าง route จากตัวอย่างเราสร้าง resource route สำหรับ examples

```php
// routes/web.php

<?php

use Illuminate\Support\Facades\Route;

...

Route::resource('examples', \App\Http\Controllers\Document\ExampleController::class)->only([
    'index', 'show', 'store', 'update', 'destroy',
]);   

```

- สร้าง controller ให้ match กับ routes ที่เรากำหนด

```php
// app/Http/Controllers/ExampleController.php

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Requests\DummyRequest;
use App\Http\Controllers\Requests\DummyUpdateRequest;
use App\Http\Controllers\Controller;

class ExampleController extends Controller
{
    public function index(Request $request)
    {
        // no code no bug
    }

    public function show(int $id)
    {
        // no code no bug
    }

    public function store(DummyRequest $request)
    {
        // no code no bug
    }

    public function update(DummyUpdateRequest $request, int $id)
    {
        // no code no bug
    }

    public function destroy(int $id)
    {
        // no code no bug
    }

}

```

- แนะนำให้สร้าง Request file เพื่อที่เราจะได้สามารถเอาไปใช้กับ api docs ได้ง่ายๆ

```php
// app/Http/Controllers/Document/Requests/DummyRequest.php

<?php

namespace App\Http\Controllers\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DummyRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
        ];
    }
}

```

- จากนั้นสร้าง Test file และในนั้นให้เราใช้ maqe api docs class เพ่ือสร้าง response ไฟล์ที่เราจะนำไปใช้แสดงใน api docs
  - ประกาศตัวแปรแบบ static จากนั้นให้ initail instance ใน `setUpBeforeClass`
  - เทสโดยการเรียก url และนำ response ที่ได้รับมาสร้าง response json file เพื่อใช้ในการสร้าง api docs ต่อไป
  - การสร้างไฟล์จะเกิดขึ้นท้ายสุดดังนั้นให้รันคำสั่ง write respones ใน function `tearDown`

```php
// tests/Feature/MaqeApiDocsExampleTest.php

<?php

namespace Tests\Feature;

use Illuminate\Http\Response;

use Maqe\MaqeApiDocs\Docs;
use Tests\TestCase;

class MaqeApiDocsExampleTest extends TestCase
{
    private static Docs $maqeApiDocs;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$maqeApiDocs = new Docs;
    }

    public function test_document_example_index_()
    {
        $response = $this->get('/examples');

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.index',
            $response,
            '200 OK'
        );
    }

    public function test_document_example_index_with_query_string()
    {
        $response = $this->get('/examples?q=2');

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.index',
            $response,
            '200 OK (with query string)'
        );
    }

    public function test_document_example_show()
    {
        $response = $this->get('/examples/2');

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.show',
            $response,
            '200 OK'
        );
    }

    public function test_document_example_show_not_found()
    {
        $response = $this->get('/examples/9999999999');

        $response->assertStatus(Response::HTTP_NOT_FOUND);

        self::$maqeApiDocs->saveResponse(
            'examples.show',
            $response,
            '404 NOT FOUND'
        );
    }

    public function test_document_example_created()
    {
        $response = $this->post('/examples', [
            'name' => 'name1',
        ]);

        $response->assertStatus(Response::HTTP_CREATED);

        self::$maqeApiDocs->saveResponse(
            'examples.store',
            $response,
            '201 CREATED'
        );
    }

    public function test_document_example_updated()
    {
        $response = $this->put('/examples/1', [
            'name' => 'nameUpdate',
        ]);

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.update',
            $response,
            '200 OK'
        );
    }

    public function test_document_example_deleted()
    {
        $response = $this->delete('/examples/1');

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.destroy',
            $response,
            '200 OK'
        );
    }

    public function tearDown(): void
    {
        self::$maqeApiDocs->writeResponse();
        parent::tearDown();
    }
}

```

- สร้าง Docuemnt page โดยให้เราสร้างไฟล์ใน folder pages `resources/views/maqe-api-docs/pages/<name>.blade.php` หรือ `resources/views/maqe-api-docs/pages/<folder_name>/<name>.blade.php`

```html

<h1 class="display-4">Example API</h1>

<h3># GET</h3>
<h4>Index</h4>
@include('maqe-api-docs.endpoint', [
    'name' => 'examples.index',
    'queries' => [
        'q' => [
            'rule' => 'string', 'description: query string for search by keyword',
        ],
    ],

])
<hr class="mb-4" />

<h3># GET</h3>
<h4>Show</h4>
@include('maqe-api-docs.endpoint', ['name' => 'examples.show'])
<hr class="mb-4" />

<h3># POST</h3>
@include('maqe-api-docs.endpoint', [
    'name' => 'examples.store',
    'request' => App\Http\Controllers\Requests\DummyRequest::class,
])
<hr class="mb-4" />

<hr class="mb-4" />
<h3># PUT</h3>
@include('maqe-api-docs.endpoint', [
    'name' => 'examples.update',
    'request' => App\Http\Controllers\Requests\DummyUpdateRequest::class,
])
<hr class="mb-4" />

<h3># DELETE</h3>
@include('maqe-api-docs.endpoint', ['name' => 'examples.destroy'])

```

> โค้ด `@include('documents.endpoint', [])` ใช้เพื่อ render ข้อมูล ตาม route ที่เรากำหนด

> โดยที่ `'name' => 'controller.name'` ซึ่ง `controller.name` ก็คือชื่อของ route ของเรา **หากไม่แน่ในว่า route ที่เราสร้างชื่ออะไรก็สามารถรันคำสั่ง `php artisan route:list` เพื่อเช็คได้

> จากที่เคยได้กล่าวมาก่อนหน้านี้ให้เราสร้าง Request class ขึ้นมาก็เพื่อที่เราจะได้สามารถนำมาใช้ได้ง่ายๆ หากเราต้องการเพิ่มข้อมูล parameters อย่าง `'request' => Request::class` โดยจากตัวอย่าง `Request::class` ก็คือ `App\Http\Controllers\Requests\DummyRequest::class` และ `App\Http\Controllers\Requests\DummyUpdateRequest::class`

> นอกจากนั้นเรายังมี `queries` ที่สามารถกำหนดเพ่ิมเติมสำหรับ แสดงผลใน api docs ได้อีกด้วย

```html
@include('maqe-api-docs.endpoint', [
    'name' => 'examples.index',
    'queries' => [
        'q' => [
            'rule' => 'string', 'description: query string for search by keyword',
        ],
    ],

])
```
 
 > การทำ custom response ในบางกรณีที่เราต้องการที่จะใช้ชื่อที่แตกต่าง จาก route ของเรา เราก็สามารถที่จะกำหนด response ได้เองโดย

 ```html
 @include('documents.endpoint', ['name' => 'passport.token', 'response' => 'passport.token.guest'])
 ```

- จัดการกับ config

```php
// config/maqe-api-docs.php

<?php

return [

    // ใช้สำหรับแสดง title และ subtitle ใน api docs
    'info' => [
        'title' => 'MAQE API Document',
        'description' => 'MAQE API Documentation description',
    ],
    // 
    'pages' => [
        'Example' => [
            'exampleFolder-examples' => 'Example API',
        ],
        'Example No Folder' => [
            'examples' => 'Example API',
        ],
    ],
];

```

จากตัวอย่าง การกำหนด ข้อมูลภายใตั `pages` จะไปแสดงผลที่ sidebar โดยจากตัวอย่างด้านบน ก็แสดงผลและลิงค์ไปยังไฟล์ที่กำหนด

  - Example
    - Example API (จะเป็นลิงค์เมื่อกดจะพาเราไปยัง ไฟล์ `./resources/views/maqe-api-docs/pages/exampleFolder/examples.blade.php`)
  - Example No Folder
    - Example API (จะเป็นลิงค์เมื่อกดจะพาเราไปยัง ไฟล์ `./resources/views/maqe-api-docs/pages/examples.blade.php`)

>Happy Coding จ๊ะ