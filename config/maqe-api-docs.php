<?php

return [
    /*
    |--------------------------------------------------------------------------
    | API Docs Pages Name
    |--------------------------------------------------------------------------
    */

    'info' => [
        'title' => 'MAQE API Document',
        'description' => 'MAQE API Documentation description',
    ],
    'pages' => [
        'Example' => [
            'exampleFolder-examples' => 'Example API',
        ],
        'Example No Folder' => [
            'examples' => 'Example API',
        ],
    ],
];
