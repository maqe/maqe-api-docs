<?php

namespace Maqe\MaqeApiDocs\Facades;

use Illuminate\Support\Facades\Facade;

class Docs extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'maqe.api.docs';
    }
}
