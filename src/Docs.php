<?php

namespace Maqe\MaqeApiDocs;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\TestResponse;

class Docs
{
    private array $responses;

    public function commitInfo()
    {
        $path = base_path('.gitlog');

        if (! file_exists($path)) {
            return [
                'commit_id' => '-',
                'time' => '-',
                'time_diff' => '-',
            ];
        }

        $log = file_get_contents($path);
        list($commentId, $time) = explode('|', $log);
        $time = Carbon::createFromTimestamp($time);

        return [
            'commit_id' => $commentId,
            'time' => $time->toRssString(),
            'time_diff' => $time->diffForHumans(),
        ];
    }

    public function createResponseJsonFile(string $route, TestResponse $response, string $name = '200 OK'): bool
    {
        try {
            $responses[] = [
                'name' => $name,
                'response' => $response->getContent(),
            ];

            $path = storage_path('docs/response/' . $route . '.json');

            Storage::disk('root')->put($path, json_encode($responses));

            return true;
        } catch (\Exception $e) {
            Log::error("Maqe api docs createResponseJsonFile [Route: {$route}] Error:: {$e->getMessage()}");
            return false;
        }
    }

    public function saveResponse(string $route, TestResponse $response, string $name = '200 OK'): Docs
    {
        $this->responses[$route][] = [
            'name' => $name,
            'response' => $response->getContent(),
        ];

        return $this;
    }

    public function writeResponse(): Docs
    {
        foreach ($this->responses as $route => $response) {
            $path = storage_path('docs/response/' . $route . '.json');

            Storage::disk('root')
            ->put($path, json_encode($response));
        }

        return $this;
    }
}
