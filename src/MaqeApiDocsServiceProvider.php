<?php

namespace Maqe\MaqeApiDocs;

// use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class MaqeApiDocsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerContainers();
        $this->registerMergeConfig();
    }

    public function boot()
    {
        $this->configurePublishing();
        $this->configureCommands();
    }

    private function registerMergeConfig()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/filesystems.php', 'filesystems.disks');
    }

    private function registerContainers()
    {
        $this->app->bind('maqe.api.docs', function ($app) {
            return new Docs;
        });
    }

    /**
     * Configure publishing for the package.
     *
     * @return void
     */
    protected function configurePublishing()
    {
        if (! $this->app->runningInConsole()) {
            return;
        }

        $this->publishes([
            __DIR__ . '/../config/maqe-api-docs.php' => config_path('maqe-api-docs.php'),
        ], 'maqe-api-docs-config');

        $this->publishes([
            __DIR__ . '/../.scripts/gitlog.sh' => base_path('.scripts/gitlog.sh'),
        ], 'maqe-api-docs-script');

        $this->publishes([
            __DIR__ . '/../stubs/resources/views/maqe-api-docs' => resource_path('views/maqe-api-docs'),
        ], 'maqe-api-docs-views');

        $this->publishes([
            __DIR__ . '/../routes/maqe-api-docs.php' => base_path('routes/maqe-api-docs.php'),
        ], 'maqe-api-docs-routes');
    }

    /**
     * Configure the commands offered by the application.
     *
     * @return void
     */
    protected function configureCommands()
    {
        if (! $this->app->runningInConsole()) {
            return;
        }

        $this->commands([
            Console\InstallCommand::class,
        ]);
    }
}
