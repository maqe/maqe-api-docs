<?php

namespace Maqe\MaqeApiDocs\Http\Controllers\Document;

use Maqe\MaqeApiDocs\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function index()
    {
        $pages = config('maqe-api-docs.pages');

        return view('maqe-api-docs.index', [
            'pages' => $pages,
        ]);
    }
}
