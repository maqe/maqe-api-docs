<?php

namespace Maqe\MaqeApiDocs\Http\Controllers\Document;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Maqe\MaqeApiDocs\Http\Controllers\Document\Requests\DummyRequest;
use Maqe\MaqeApiDocs\Http\Controllers\Document\Requests\DummyUpdateRequest;
use Maqe\MaqeApiDocs\Http\Controllers\Controller;

class ExampleController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->get('q', null);

        $responses = $this->getData(20);

        if ($q === null) {
            return response()->json(['data' => $responses], Response::HTTP_OK);
        }

        $responses = $responses->filter(function ($item) use ($q) {
            return false !== stristr($item['name'], $q);
        });

        if ($responses !== null) {
            return response()->json(['data' => $responses], Response::HTTP_OK);
        }

        return response()->json([
            'error' => 'not_found',
            'message' => 'The resource is not found.',
        ], Response::HTTP_NOT_FOUND);
    }

    public function show(int $id)
    {
        $data = $this->getData(20);
        $response = $data->firstWhere('id', $id);

        if (empty($response) === true) {
            return response()->json([
                'error' => 'not_found',
                'message' => 'The resource is not found.',
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json(['data' => $response], Response::HTTP_OK);
    }

    public function store(DummyRequest $request)
    {
        $name = $request->only(['name']);

        // Assume that stored succeed
        $response = [
            'id' => 1,
            'name' => $name,
        ];

        return response()->json(['data' => $response], Response::HTTP_CREATED);
    }

    public function update(DummyUpdateRequest $request, int $id)
    {
        $name = $request->only(['name']);

        // Assume that updated succeed
        $response = [
            'id' => 1,
            'name' => $name,
        ];

        return response()->json(['data' => $response], Response::HTTP_OK);
    }

    public function destroy(int $id)
    {
        // Assume that deleted succeed
        return response()->json([], Response::HTTP_OK);
    }

    private function getData(int $total = 10)
    {
        $input = [];

        foreach (range(0, $total) as $index) {
            $input[] = [
                'id' => $index,
                'name' => "name_{$index}",
            ];
        }

        return collect($input);
    }
}
