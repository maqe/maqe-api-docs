<?php

namespace Maqe\MaqeApiDocs\Http\Controllers\Document\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DummyRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
        ];
    }
}
