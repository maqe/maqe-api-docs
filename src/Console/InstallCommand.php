<?php

namespace Maqe\MaqeApiDocs\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maqe-api-docs:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the Maqe Api Docs scripts, config and resources';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // Publish...

        $this->callSilent('vendor:publish', ['--tag' => 'maqe-api-docs-config', '--force' => true]);
        $this->callSilent('vendor:publish', ['--tag' => 'maqe-api-docs-script', '--force' => true]);
        $this->callSilent('vendor:publish', ['--tag' => 'maqe-api-docs-views', '--force' => true]);
        // $this->callSilent('vendor:publish', ['--tag' => 'maqe-api-docs-routes', '--force' => true]);

        // Routes...
        if (! Str::contains(file_get_contents(base_path('routes/web.php')), "'docs'")) {
            (new Filesystem)->append(base_path('routes/web.php'), $this->maqeApiDocsRouteDefinition());
        }

        // Tests...
        copy(__DIR__ . '/../../stubs/tests/MaqeApiDocsExampleTest.php', base_path('tests/Feature/MaqeApiDocsExampleTest.php'));
    }

    /**
     * Get the route definition(s) that should be installed for Livewire.
     *
     * @return string
     */
    protected function maqeApiDocsRouteDefinition()
    {
        return <<<'EOF'

Route::get('docs', [\Maqe\MaqeApiDocs\Http\Controllers\Document\DocumentController::class, 'index'])->name('docs');

Route::resource('examples', \Maqe\MaqeApiDocs\Http\Controllers\Document\ExampleController::class)->only([
    'index', 'show', 'store', 'update', 'destroy',
]);        

EOF;
    }
}
