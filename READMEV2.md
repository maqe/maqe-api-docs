# IDEA FOR IMPROVEMENT FOR VERSION 2

## PAIN POINTS

@Panupong Mongkoladisai

more issue from version using in BNN

so many hardcode string

- route name
- file name
- mapping in config

nested folder

- currently use - as sub folder naming for doc page file
- if we can use sub array or other way that reflect on folder structure, that would be helpful
- or if we can just remove config file that will be the best
## IDEAS

@Panupong Mongkoladisai

something I want to change

- instead of using ApiTester as static class, create it as another  based test ApiTestCase
- any api testing will inherit from this

why?

- reduce boiler plate of static setup/teardown

warning

- this will be come 2 layer inheritance which need to be careful not to make it too complicate

my imagination
- you write a test that define
    - route it is testing
    - request class it is using
    - the data it is sending
    - the page doc path it is saving to
- after the test run it save all of it in the result file
- then the doc page read that result and generate view

with this you can remove

- manual request binding in view
- config file