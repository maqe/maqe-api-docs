@extends('maqe-api-docs.layout')

@include('maqe-api-docs.nav', ['pages' => $pages])

@section('content')
    <div class="tab-content" id="tabContent">

        @foreach ($pages as $topic => $links)
            @foreach ($links as $key => $value)
                @if ($loop->parent->first && $loop->first)
                    <div class="tab-pane show active" id="{{ $key }}" role="tabpanel">
                        @include('maqe-api-docs.pages.' . str_replace('-', '.', $key))
                    </div>
                @else
                    @if(!is_array($value))
                        <div class="tab-pane show" id="{{ $key }}" role="tabpanel">
                            @include('maqe-api-docs.pages.' . str_replace('-', '.', $key))
                        </div>
                    @else
                        @foreach($value as $k => $l)
                        <div class="tab-pane show" id="{{ $k }}" role="tabpanel">
                            @include('maqe-api-docs.pages.' . str_replace('-', '.', $k))
                        </div>
                        @endforeach
                    @endif
                @endif
            @endforeach
        @endforeach
    </div>
@endsection
