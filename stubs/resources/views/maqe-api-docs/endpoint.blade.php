@php
    $routes = \Illuminate\Support\Facades\Route::getRoutes();
    
    $route = $routes->getByName($name);

    if (is_null($route)) {
        throw new InvalidArgumentException('There is no route name "' . $name . '".');
    }

    $key = str_replace('.', '-', $name);

    $requireClientToken = in_array('client', $route->action['middleware']);
    $requireUserToken = in_array('auth:api', $route->action['middleware']);

    $requireAuth = $requireUserToken;
    $request = isset($request) ? new $request : null;
    $responseName= isset($response) ? $response : $name;

    try {
        $response = file_get_contents(storage_path('docs/response/'.$responseName.'.json'));
        $jsonResponse = json_decode($response);
    } catch (\Exception $exception) {
        $jsonResponse = 'Response for '.storage_path('docs/response/'.$responseName.'.json').' is generating..., please try again in a moment.';
    }
@endphp
<p class="lead bg-light p-3">
    @include('maqe-api-docs.components.method', ['names' => $route->methods])
    /{{ $route->uri }}
</p>

@if (!is_null($request))
    <h6>Attributes</h6>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Rules</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($request->rules() as $keyRule => $rules)
            <tr>
                <td class="w-25">
                    <code>{{ $keyRule }}</code>
                </td>
                <td class="w-75">
                    @foreach ($rules as $rule)
                        <pre class="mb-0">{{ $rule }}</pre>
                    @endforeach
                </td>
            </tr>
        @endforeach
    </table>
@endif

@if (isset($queries))
    <h6>Queries</h6>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Values</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($queries as $keyRule => $rules)
            <tr>
                <td class="w-25">
                    <code>{{ $keyRule }}</code>
                </td>
                <td class="w-75">
                    @foreach ($rules as $rule)
                        <pre class="mb-0">{{ $rule }}</pre>
                    @endforeach
                </td>
            </tr>
        @endforeach
    </table>
@endif

@if (isset($statuses))
<table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">Status</th>
            <th scope="col">Wording</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($statuses as $status => $wording)
        <tr>
            <td class="w-25">
                <code>{{ $status }}</code>
            </td>
            <td class="w-75">
                @foreach ($wording as $word)
                <pre class="mb-0">{{ $word }}</pre>
                @endforeach
            </td>
        </tr>
        @endforeach
</table>
@endif

@if (is_string($jsonResponse))
    <p>{{$jsonResponse}}</p>
@else
    @foreach ($jsonResponse as $index => $response)
    <div class="accordion" id="accordionExample">
        <div class="card border-0 rounded">
            <div class="card-header p-0 bg-dark text-light" id="heading-{{ $key }}-{{ $index }}">
                <h2 class="mb-0">
                    <button class="btn btn-link text-warning btn-block text-left text-decoration-none" type="button"
                            data-toggle="collapse" data-target="#collapse-{{ $key }}-{{ $index }}" aria-expanded="true"
                            aria-controls="collapse-{{ $key }}-{{ $index }}">
                        {{ $response->name }}
                    </button>
                </h2>
            </div>

            <div id="collapse-{{ $key }}-{{ $index }}" class="collapse hide" aria-labelledby="heading-{{ $key }}-{{ $index }}"
                data-parent="#heading-{{ $key }}-{{ $index }}">
                <pre class="card-body bg-dark text-light">{{ json_encode(json_decode($response->response), JSON_PRETTY_PRINT) }}</pre>
            </div>
        </div>
    </div>
    <br>
    @endforeach
@endif
