@foreach ($names as $name)
    @switch($name)
        @case('GET')
        <span class="badge badge-primary">GET</span>
        @break

        @case('POST')
        <span class="badge badge-success">POST</span>
        @break

        @case('PUT')
        <span class="badge badge-warning">PUT</span>
        @break

        @case('DELETE')
        <span class="badge badge-danger">DELETE</span>
        @break

    @endswitch
@endforeach