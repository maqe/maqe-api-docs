<h6>Attributes</h6>
<table class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Rules</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($body as $keyRule => $rules)
        <tr>
            <td class="w-25">
                <code>{{ $keyRule }}</code>
            </td>
            <td class="w-75">
                @foreach ($rules as $rule)
                    <pre class="mb-0">{{ $rule }}</pre>
                @endforeach
            </td>
        </tr>
    @endforeach
</table>
