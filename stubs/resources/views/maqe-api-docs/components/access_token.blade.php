@if ($client)
    <span class="badge badge-dark">GUEST_ACCESS_TOKEN</span>
@endif
@if ($user)
    <span class="badge badge-dark">USER_ACCESS_TOKEN</span>
@endif

@if (!$client && !$user)
    <span class="badge badge-light">NO_ACCESS_TOKEN</span>
@endif