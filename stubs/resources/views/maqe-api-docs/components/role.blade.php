@if ($auth)
    @if ($admin)
        <span class="badge badge-dark">ADMIN</span>
    @endif
    @if ($seller)
        <span class="badge badge-primary">SELLER</span>
    @endif
    @if ($buyer)
        <span class="badge badge-warning">BUYER</span>
    @endif

    @if (!$admin && !$seller && !$buyer)
        <span class="badge badge-dark">ADMIN</span>
        <span class="badge badge-primary">SELLER</span>
        <span class="badge badge-warning">BUYER</span>
    @endif
@else
    <span class="badge badge-secondary">GUEST</span>
@endif