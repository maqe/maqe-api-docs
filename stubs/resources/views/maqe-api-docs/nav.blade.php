@php
$info = \Maqe\MaqeApiDocs\Facades\Docs::commitInfo();
@endphp

@section('nav')

    <div class="card mb-3">
        <div class="card-body p-3">
            <p class="m-0">
                <strong>Version:</strong> {{ $info['commit_id'] }}<br />
                <strong>Updated:</strong> <span title="{{ $info['time'] }}">{{ $info['time_diff'] }}</span>
            </p>
        </div>
    </div>

    <div id="nav" class="nav flex-column" role="tablist" aria-orientation="vertical">
        @foreach ($pages as $topic => $links)
            @if ($loop->first)
                <strong>{{ $topic }}</strong>
            @else
                <strong class="mt-3">{{ $topic }}</strong>
            @endif
            @foreach ($links as $key => $link)
                @if ($loop->parent->first && $loop->first)
                    <a id="{{ $key }}-tab" data-toggle="pill" href="#{{ $key }}" role="tab"
                       aria-selected="true" class="nav-link active p-0">{{ $link }}</a>
                @elseif (is_array($link))
                {{$key}}
                        @foreach($link as $key => $link)
                            <a id="{{ $key }}-tab" data-toggle="pill" href="#{{ $key }}" role="tab"
                            aria-selected="false" class="nav-link p-0 pl-3">{{ $link }}</a>
                        @endforeach

                @else
                    <a id="{{ $key }}-tab" data-toggle="pill" href="#{{ $key }}" role="tab"
                       aria-selected="false" class="nav-link p-0">{{ $link }}</a>
                @endif
            @endforeach
        @endforeach
    </div>

@endsection
