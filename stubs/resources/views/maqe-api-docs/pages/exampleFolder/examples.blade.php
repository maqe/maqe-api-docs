
<h1 class="display-4">Example API</h1>

<h3># GET</h3>
<h4>Index</h4>
@include('maqe-api-docs.endpoint', [
    'name' => 'examples.index',
    'queries' => [
        'q' => [
            'rule' => 'string', 'description: query string for search by keyword',
        ],
    ],

])
<hr class="mb-4" />

<h3># GET</h3>
<h4>Show</h4>
@include('maqe-api-docs.endpoint', ['name' => 'examples.show'])
<hr class="mb-4" />

<h3># POST</h3>
@include('maqe-api-docs.endpoint', [
    'name' => 'examples.store',
    'request' => Maqe\MaqeApiDocs\Http\Controllers\Document\Requests\DummyRequest::class,
])
<hr class="mb-4" />

<hr class="mb-4" />
<h3># PUT</h3>
@include('maqe-api-docs.endpoint', [
    'name' => 'examples.update',
    'request' => Maqe\MaqeApiDocs\Http\Controllers\Document\Requests\DummyUpdateRequest::class,
])
<hr class="mb-4" />

<h3># DELETE</h3>
@include('maqe-api-docs.endpoint', ['name' => 'examples.destroy'])
