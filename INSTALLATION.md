# BASE INSTALLATION

ณ ปัจจุบันนี้เรายังใช้งาน private repository อยู่ดังนั้นกรุณาติดตั้ง package ตามวิธีด้านล่างนี้

1. เพิ่มโค้ดใน `composer.json` 
```json
{
    "repositories": {
        "local": {
            "type": "vcs",
            "url": "git@bitbucket.org:maqe/maqe-api-docs.git"
        }
    }
}
```

> การ install ด้วย private repository ต้องไปเพิ่ม SSH keys ใน bitbucket ด้วยหากใช้ url สำหรับ SSH

2. รันคำสั่งเพื่อติดตั้งใน autoload

```sh
composer require maqe/maqe-api-docs
```

3. รันคำสั่ง Install ซึ่งจะทำการ setup config และ publish ไฟล์ต่างๆที่จำเป็นให้กับเราโดยอัตโนมัติ

```sh
php artisan maqe-api-docs:install
```

4. ทดสอบ example route โดยเข้าไปที่ `http://your_host/docs`
