<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Maqe\MaqeApiDocs\Docs as MaqeApiDocsDocs;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    // protected $maqeApiDocs;
    private static MaqeApiDocsDocs $maqeApiDocs;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$maqeApiDocs = new MaqeApiDocsDocs;
    }

    public function test_document_example_index_()
    {
        $response = $this->get('/examples');

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.index',
            $response,
            '200 OK'
        );
    }

    public function test_document_example_index_with_query_string()
    {
        $response = $this->get('/examples?q=2');

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.index',
            $response,
            '200 OK (with query string)'
        );
    }

    public function test_document_example_show()
    {
        $response = $this->get('/examples/2');

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.show',
            $response,
            '200 OK'
        );
    }

    public function test_document_example_show_not_found()
    {
        $response = $this->get('/examples/9999999999');

        $response->assertStatus(Response::HTTP_NOT_FOUND);

        self::$maqeApiDocs->saveResponse(
            'examples.show',
            $response,
            '404 NOT FOUND'
        );
    }

    public function test_document_example_created()
    {
        $response = $this->post('/examples', [
            'name' => 'name1',
        ]);

        $response->assertStatus(Response::HTTP_CREATED);

        self::$maqeApiDocs->saveResponse(
            'examples.store',
            $response,
            '201 CREATED'
        );
    }

    public function test_document_example_updated()
    {
        $response = $this->put('/examples/1', [
            'name' => 'nameUpdate',
        ]);

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.update',
            $response,
            '200 OK'
        );
    }

    public function test_document_example_deleted()
    {
        $response = $this->delete('/examples/1');

        $response->assertStatus(Response::HTTP_OK);

        self::$maqeApiDocs->saveResponse(
            'examples.destroy',
            $response,
            '200 OK'
        );
    }

    public function tearDown(): void
    {
        self::$maqeApiDocs->writeResponse();
        parent::tearDown();
    }
}
